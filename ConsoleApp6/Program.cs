﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                int[] arr = { 1, 1, 1, 55, 8, 8, 8, -5, 15, 15, 15 };

                #region Get Max and Index
                int max = arr[0];
                int maxIndex = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i] > max)
                    {
                        max = arr[i];
                        maxIndex = i;
                    }
                }
                #endregion
                #region Get Min and Index
                int min = arr[0];
                int minIndex = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i] < min)
                    {
                        min = arr[i];
                        minIndex = i;
                    }
                }
                #endregion
                #region Which one is found first.
                for (int i = 0; i < arr.Length; i++)
                {
                    // Minimum is first one.
                    if (minIndex < maxIndex)
                    {
                        if (i < minIndex)
                        {
                            arr[i] = 0;
                        }
                        else if (i > minIndex && i < maxIndex)
                        {
                            arr[i] = 1;
                        }
                        else if (i > maxIndex)
                        {
                            arr[i] = 2;
                        }
                        Console.WriteLine(arr[i]);
                    }
                    // Maximum is first one.
                    else if (minIndex > maxIndex)
                    {
                        if (i < maxIndex)
                        {
                            arr[i] = 0;
                        }
                        else if (i > maxIndex && i < minIndex)
                        {
                            arr[i] = 1;
                        }
                        else if (i > minIndex)
                        {
                            arr[i] = 2;
                        }
                        Console.WriteLine(arr[i]);
                    }
                    // Only one digit or all same.
                    else
                    {
                        Console.WriteLine("All the numbers are same, or there is only one number in this array.");
                        break;
                    }
                }
                #endregion
                Console.ReadLine();
            }
        }
    }
}
