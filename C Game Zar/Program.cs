﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_Game_Zar
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 298;
            int[] counter = new int[7];
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                int num = rnd.Next(1, 7);
                counter[num]++;
            }

            Console.WriteLine($"1: {counter[1]}");
            Console.WriteLine($"2: {counter[2]}");
            Console.WriteLine($"3: {counter[3]}");
            Console.WriteLine($"4: {counter[4]}");
            Console.WriteLine($"5: {counter[5]}");
            Console.WriteLine($"6: {counter[6]}");

            int max = counter[0];
            for (int i = 0; i < counter.Length; i++)
            {
                if (counter[i] > max)
                {
                    max = counter[i];
                }
            }
            Console.WriteLine($"max={max}");
            Console.ReadLine();
        }
    }
}
