﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D_Tvi_Max_Nshan_
{
    class Program
    {
        static void Main(string[] args)
        {//1. Trvac e tiv, gtnel amenamec tvanshan@;
            
            int n = -123;

            #region separate and Get Max
            int max= int.MinValue;
            
            while (n != 0)
            {
               int rem = n % 10;
                n /= 10;
                // Get Max
                if (rem>max)
                {
                    max = rem;
                }
            }
            #endregion

           
            //print
            Console.WriteLine($" max={max}");
            Console.ReadLine();
        }
    }
}
