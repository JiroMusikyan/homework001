﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G_getAraySumMethods
{
    class Program
    {
        static int getSum(int[]arg)
        #region getArayAmount
        {
            int sum = 0;
            for (int i = 0; i < arg.Length; i++)
            {
                sum += arg[i];
            }
            return sum;
        }
        #endregion

        static void Main(string[] args)
        {
            Random rnd = new Random();

            int[] arr = new int[31];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(7, 64);
            }

            int sum = getSum(arr);

            Console.WriteLine($"Aray element amount is {sum}");
            Console.ReadLine();

        }
    }
}
