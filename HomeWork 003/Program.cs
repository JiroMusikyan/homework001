﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_003
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 12, 34, 76, 89, 98 };

            for (int i = arr.Length-1; i > 0; i--)
            {
                Console.Write($"{arr[i]},");
            }
            Console.ReadLine();
        }
    }
}
