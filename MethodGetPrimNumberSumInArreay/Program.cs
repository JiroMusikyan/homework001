﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Program
    {
        static bool Even(int arg)
        {
            if (arg % 2 == 0)
            {
                return true;
            }
            return false;
        }

        static int Sum(int[] arg)
        {
            int sum = 0;
            for (int i = 0; i < arg.Length; i++)
            {
                int a = arg[i];
                if (GetPrime(a))
                {
                    sum += a;
                }
            }
            return sum;
        }

        static bool GetPrime(int arg)
        {
            if (arg < 2)
                return false;
            if (arg == 2)
                return true;
            if (arg % 2 == 0)
                return false;
            // double sqrArg = Math.Sqrt(arg);
            for (int div = 3; div * div < arg; div += 2)
            {
                if (arg % div == 0)
                {
                    return false;
                }
            }
            return true;

        }
        static void Main(string[] args)
        {
            int[] arr = new int[] { 1, 4, 33, 12, 1 };
            int x = Sum(arr);

            Console.WriteLine(x);
            Console.ReadLine();
        }
    }
}
