﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G_get_ArrMinElement
{
    class Program
    {
        static int getMinEle(int[]arg)
        {
            int min = int.MaxValue;
            for (int i = 0; i < arg.Length; i++)
            {
                if (arg[i]<min)
                {
                    min = arg[i];
                }
            }
            return min;
        }
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int[] arr = new int[12];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(1, 86);
            }
            int min = getMinEle(arr);
            Console.WriteLine($"the min element in Arey is  {min}");
            Console.ReadLine();
        }
    }
}
