﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetMaxTwoArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] array = CreatRandomArray(2, 3);
            int max = GetMax(array);
            Print(max);
            Console.ReadLine();
        }

        static int[,] CreatRandomArray(int x, int y)
        {
            var array = new int[x, y];

            var rnd = new Random();
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    array[i, j] = rnd.Next(9, 99);
                    Console.WriteLine(array[i,j]);
                }
                Console.WriteLine("\t");
            }
            return array;
        }

        static int GetMax(int[,] arg)
        {
            int x = arg.GetLength(0);
            int y = arg.GetLength(1);
                    int max = int.MinValue;

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    int e = arg[i, j];
                    if (e>max)
                    {
                        max = e;
                    }
                }
            }
            
            return max;
        }
        static void Print(int arg)
        {
            Console.WriteLine($"max value is{arg}");
        }
    }
}
