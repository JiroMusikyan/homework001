﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassAddress

{
    class Program
    {
        static void Main(string[] args)
        {
           
            //Address address = new Address();
            //address.Country = "Armenia";
            //address.City = "Yerevan";
            //address.Street = "G.Zohrab";
            //address.House = 117;
            //address.Apartment = 9;
            //Address address2 = new Address
            //{
            //    Country = "USA",
            //    City = "Seattle",
            //    Street = "Denny Way",
            //    House = 1120,
            //};
            //address2.Show();
            //Console.WriteLine();
            //address.Show();

            //   Console.WriteLine(address.Show());
            Console.ReadLine();
        }

    }

    public class Address
    {
        public Address(int index, string country, string city, string street, uint house, byte apartment)
        {
            Index = index;
            Country = country;
            City = city;
            Street = street;
            House = house;
            Apartment = apartment;
        }
        public Address()
        {

        }
        public int Index { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public uint House { get; set; }
        public byte Apartment { get; set; }
        public void Show()
        {
            Console.WriteLine($"post indes--{Index}\ncountri--{Country}\ncity--{City}");
            Console.WriteLine($"street --{Street}\nhouse--{House}\napatment--{Apartment}");
        }

    }
}

