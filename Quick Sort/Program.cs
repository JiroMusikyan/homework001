﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quick_Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] { -5, 6, -1, 5, 43, 7, -12, 4 };
            QuickSort(array, 0, array.Length - 1);
            foreach (var item in array)
            {
                Console.Write($"{item},");
            }

            Console.ReadLine();
        }

        static void QuickSort(int[] array, int left, int right)
        {
            int i = left;
            int j = right;
            int pivot = array[(left + right) >> 1];// /2;
            while (i <= j)
            {
                while (array[i] < pivot)
                {
                    i++;
                }
                while (array[j] > pivot)
                {
                    j--;
                }
                if (i <= j)
                {
                    int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                    i++;
                    j--;
                }
            }
            //Recursive Calls;
            if (left < j)
            {
                QuickSort(array, left, j);
            }
            if (i < right)
            {
                QuickSort(array, i, right);
            }
        }

    }
}
