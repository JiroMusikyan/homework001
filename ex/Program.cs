﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 2, 14, 99, 8, 31, 67, 92, 23, 13 };

            int max = 0;
            int maxIndex = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                int sum = 0;
                while (arr[i] != 0)
                {
                    int a = arr[i] % 10;
                    sum += a;
                    arr[i] /= 10;

                }

                if (max < sum)
                {
                    max = sum;
                    maxIndex = i;
                }
            }

            Console.WriteLine(max);
            Console.WriteLine(maxIndex);
            Console.ReadLine();
        }
    }
}
