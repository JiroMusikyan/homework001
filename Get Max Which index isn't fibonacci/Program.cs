﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Get_Max_Which_index_isn_t_fibonacci
{
    class Program
    {

        static void Main(string[] args)
        {
            int[] arr = { 1, 10000, 21112, 31000, 5, 51111, 7, 8, 81111, 0, 12, 23, 44, 139955, 63, 22, 2, 6 };
            //              0   1      2      3     4   5    6  7     8    9 10  11  12    13    14  15  16 17
            int fib1 = 1;
            int fib2 = 1;
            int fib = 1;
            int max = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (i == fib)
                {
                    fib = fib1 + fib2;
                    fib1 = fib2;
                    fib2 = fib;
                }
                else if(arr[i]>max)
                {
                    max =arr[i];
                }
            }

            Console.WriteLine($"Max which index is not Fibonacci {max}");
            Console.ReadLine();
        }
    }
}
