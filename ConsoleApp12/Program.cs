﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input array count: ");
            int n = int.Parse(Console.ReadLine());

            int[] arr = CreateArray(n); //{ 8,4,21,48,45};

            Print(arr);

            if (AnyPrime(arr))
            {
                int sum = GetSum(arr);
                Console.WriteLine("Prime sum is:{0}", sum);
            }
            else
            {
                int sumEven = GetSumEvenNumbers(arr);
                Console.WriteLine("Sum of even numbers is:{0}", sumEven);
            }
            Console.ReadLine();
        }

        static public int[] CreateArray(int a) //ARRAY
        {
            int[] arr = new int[a];

            Random rand = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next(0, 50);
            }
            return arr;
        }

        static void Print(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("arr{0} = {1}", i, arr[i]);
            }
        }

        static bool IsPrime(int a)
        {
            if (a < 2)
                return false;

            if (a == 2)
                return true;

            if (a % 2 == 0)
                return false;

            double sqrtA = Math.Sqrt(a);
            for (int div = 3; div <= sqrtA; div += 2)
                if (a % div == 0)
                    return false;

            return true;
        }

        static public int GetSum(int[] arr) // SUM 
        {
            int sum = 0;

            foreach (int item in arr)
            {
                sum += item;
            }
            return sum;
        }

        static public int GetSumEvenNumbers(int[] arr) // SUM 
        {
            int sum = 0;

            foreach (int item in arr)
            {
                if (item % 2 == 0)
                {
                    sum += item;
                }
            }
            return sum;
        }
        static bool AnyPrime(int[] arr)
        {
            foreach (int item in arr)
            {
                if (IsPrime(item))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
