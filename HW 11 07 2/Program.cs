﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_11_07_2
{//Zangvacum gtnum enq  Maxn  u  Minin, minchev Max@ elementneri arjeqnern poxum enq 00000000
 // minchev Minin poxum enq 11111111minic heto poxum enq 22222222 ;
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 9, 4, 8, -1000, 5, 7, 3, 1000, 15, 17, 18 };

            #region Get indexes of max and min values

            int max = arr[0];
            int min = arr[0];
            int maxIndex = 0;
            int minIndex = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                    maxIndex = i;
                }
                else if (arr[i] < min)
                {
                    min = arr[i];
                    minIndex = i;
                }
            }
            #endregion

            #region Get first and last critical points

            int firstPoint = maxIndex;
            int lastPoint = minIndex;
            if (maxIndex > minIndex)
            {
                firstPoint = minIndex;
                lastPoint = maxIndex;
            }
            #endregion

            #region Change values
            //0...0, first critical point,1...1, second critical point, 2...2
            for (int i = 0; i < firstPoint; i++)
                arr[i] = 0;
            for (int i = firstPoint + 1; i < lastPoint; i++)
                arr[i] = 1;
            for (int i = lastPoint + 1; i < arr.Length; i++)
                arr[i] = 2;
            #endregion

            // print
            for (int i = 0; i < arr.Length; i++)
                Console.Write(arr[i] + ", ");

            Console.ReadLine();
        }
    }
}