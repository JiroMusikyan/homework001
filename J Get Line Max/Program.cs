﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J_Get_Line_Max
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] array = CreatRandomArray(2, 3);
            PrintMax(array);
            Console.ReadLine();
        }
        static int[,] CreatRandomArray(int x, int y)
        {
            var array = new int[x, y];

            var rnd = new Random();
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    array[i, j] = rnd.Next(9, 99);
                    Console.WriteLine(array[i, j]);
                }
                Console.WriteLine("\t");
            }
            return array;
        }

        static void PrintMax(int[,] arg)
        {
            int x = arg.GetLength(0);
            int y = arg.GetLength(1);

            for (int i = 0; i < x; i++)
            {
                int max = int.MinValue;
                for (int j = 0; j < y; j++)
                {
                    int e = arg[i, j];
                    if (e > max)
                    {
                        max = e;
                    }
                }
                Console.WriteLine(max);
            }
        }


    }
}
