﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 12, 45, 36, 78, 17, 59 };

            long max1 = long.MinValue;
            long max2 = long.MinValue;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > max1)
                {
                    max2 = max1;
                    max1 = arr[i];
                }
                else
                    if (arr[i] > max2)
                {
                    max2 = arr[i];
                }
            }
            Console.WriteLine(max1);
            Console.WriteLine(max2);
            Console.ReadLine();
        }
    }
}
