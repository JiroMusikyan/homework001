﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JaggetArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] arr = CreateRadomJaggetArray(3);

            Console.ReadLine();
        }

        static int[][] CreateRadomJaggetArray(int count)
        {
            int[][] arr = new int[count][];
            var rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                int l = rnd.Next(1, 15);
                arr[i] = CreateRandomArray(l);
            }
            return arr;
        }

        static int[] CreateRandomArray(int count)
        {
            var arr = new int[count];

            var rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
                arr[i] = rnd.Next(10, 100);

            return arr;
        }
        
    }
}
