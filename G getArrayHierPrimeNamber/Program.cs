﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G_getMaxPrime
{
    class Program
    { 
        static void Print(int max)
        #region
        {
            if (max != 0)
                Console.WriteLine(max);
            else
                Console.WriteLine("No prime number");
        }
        #endregion

     //   static int[] CreatPandomArray(int arrlength)
     //   #region
     //   {
     //       Random rnd = new Random();
     //       int[] arr=new int[arrlength];
     //       for (int i = 0; i < arrlength; i++)
     //       {
     //           arr[i] = rnd.Next(0, 50);
     //       }
     //       return arr;
     //   }
     //   #endregion

        static int GetMax(int[] arr)
        #region
        {
            int max = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                int a = arr[i];
                if (a > max)
                {
                    if (GetPrime(a))
                        max = a;
                }
            }
            return max;
        }
        #endregion

        static bool GetPrime (int arg)
        #region
        {
            if (arg<2)
                return false;
            if (arg==2)
                return true;
            if (arg % 2 == 0)
                return false;
            double sqrArg = Math.Sqrt(arg);
            for (int div = 3; div*div <= arg; div+=2)
            {
                if (arg % div==0)
                {
                    return false;
                }
            }
                return true;
            
        }
        #endregion

        static void Main(string[] args)
        {
            int arrLength = 9;
            int[] arr = { 25, 41, 18 };// CreatPandomArray(arrLength);
            int max = GetMax(arr);
            Print(max);
           
            Console.ReadLine();
        }
    }
}
