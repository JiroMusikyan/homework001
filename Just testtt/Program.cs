﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassStudentsGrupp
{
    class Program
    {
        static void Main(string[] args)
        { 
            int grups = 4;
            Student[] students = CreateStudents(33);
           // Show(students);
            Shaffle(students);
            //Show(students);
            Student[][] x = CreatGrup(students, grups);
            Print(x);
            Console.ReadLine();
        }

        static void Shaffle(Student[] stu)
        {
            int n = stu.Length;
            var rnd = new Random();

            for (int i = 0; i < n; i++)
            {
                Swap(stu, i, i + rnd.Next(n - i));
            }

            Console.WriteLine(new string('-', 100));
        }

        static void Swap(Student[] stu, int a, int b)
        {
            Student temp = stu[a];
            stu[a] = stu[b];
            stu[b] = temp;
        }

        static Student[][] CreatGrup(Student[] stu, int div)
        {
            Student[][] grup = new Student[div][];
            int n = stu.Length;
            int grupCount = div;

            int x = n / grupCount;
            int count = 0;
            for (int i = 0; i < grup.Length; i++)
            {
                if (n <= (x + (x/2)))
                {
                    x = n;
                }
                n -= x;
                grup[i] = new Student[x];

                for (int j = 0; j < x; j++)
                {
                    grup[i][j] = stu[count];
                    count++;
                }

            }
            return grup;
        }

        static void Show(Student[] stu)
        {
            foreach (Student item in stu)
            {
                Console.WriteLine();
                item.Print();
            }

            Console.WriteLine(("").PadRight(100, '-'));
        }

        static void Print(Student[][] stu)
        {
            for (int i = 0; i < stu.Length; i++)
            {
                for (int j = 0; j < stu[i].Length; j++)
                {
                    stu[i][j].Print();
                }
                Console.WriteLine(new string('-', 100));
            }
        }

        static Student[] CreateStudents(int count)
        {
            var rnd = new Random();
            Student[] students = new Student[count];
            for (int i = 0; i < students.Length; i++)
            {
                Student stu = new Student();
                stu.name = $"A{i + 1}";
                stu.surname = $"A{i + 1}yan";
                stu.email = $"A{i + 1}@gmail.com";
                stu.age = rnd.Next(17, 40);

                students[i] = stu;
            }
            return students;
        }
    }
    public class Student
    {

        public string name;
        public string surname;
        public string email;
        public int age;
        public void Print()
        {
            Console.WriteLine($"name__{name}: surname__{surname}: email__{email}: age__{age}|");
        }
    }
}
