﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Finde_Perfect_Number
{
    class Program
    {
        static void Main(string[] args)
        {// Perfect number
            #region Arr Random 
            Random rnd = new Random();
            int[] arr = new int[10000]; 
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(100000, int.MaxValue);
            }
            #endregion

            #region get Perfect number
            for (int i = 0; i < arr.Length; i++)
            {
                int sum = 0;
                int n = arr[i];
                for (int a = 1; a <= n/2; a++)
                {
                    if (n % a == 0)
                    {
                        sum = sum + a;
                    }
                }
                if (sum == arr[i])
                {
                    Console.WriteLine($"{arr[i]} isPerfect number");
                }
            }
            #endregion

            #region Get perfect number OK!
            for (int i = 0; i < arr.Length; i++)
            {// After 100 000 up to int.maxValeu we have only one Perfect number (33550336)
                if (arr[i]== 33550336)
                {
                    Console.WriteLine(arr[i]);
                }
            }
            #endregion
            Console.ReadLine();
        }
    }
}
