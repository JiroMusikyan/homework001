﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetMaxPrime
{
    class Program
    {
        static bool IsPrime(int a)
        {
            if (a < 2)
                return false;

            if (a == 2)
                return true;

            if (a % 2 == 0)
                return false;

            double sqrtA = Math.Sqrt(a);
            for (int div = 3; div <= sqrtA; div += 2)
                if (a % div == 0)
                    return false;

            return true;
        }

        static public int GetMax(int[] arr) //MAX
        {
            int max = arr[0];
            for (int i = 1; i < arr.Length; i++)
            {
                if (max < arr[i])
                {
                    max = arr[i];
                }
            }
            return max;
        }

       

        static void Print(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("arr{0} = {1}", i, arr[i]);
            }
        }

        static void Main(string[] args)
        {

            int[] arr = { 1,1,1,1,9};

            Print(arr);
            int max;

            for (int i = 0; i < arr.Length; i++)
            {
                int a = arr[i];
                if (IsPrime(a) == false)
                {
                    arr[i] = 0;
                }
                //  Console.WriteLine(arr[i]);
            }
            max = GetMax(arr);

            Console.WriteLine("max prime number is: {0}", max);
            Console.ReadLine();
        }
    }
}