﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H_Sort_For_Byte_Array
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] array = CreateRandomArray(100000);
            SortMeth(array);
            Print(array);

            Console.ReadLine();
        }

        static void Print(byte[] b)
        {
            byte[] arr = b;
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write($"{arr[i]} ");
            }
        }

        static void SortMeth(byte[] b)
        {
            byte[] arr = b;

            const int c = 256;
            int[] countArr = new int[c];
            for (int i = 0; i < arr.Length; i++)
            {
                byte a = arr[i];
                countArr[a]++;
            }

            byte z = 0;         // mer mot Z da tvyal elementna 0,1,2 stc..
            int counter = 0;    // Counter@ arr amen hajort indexna...
            for (int i = 0; i < countArr.Length; i++)  // es cikl@ veraarjeqavorm e arr array@
            {
                for (int j = 0; j < countArr[i]; j++) // es ciklov@ X tvic X angam dnelu hamara! 
                {
                    arr[counter] = z;
                    counter++;
                }
                z++;
            }
        }

        static byte[] CreateRandomArray(int arg)
        {
            Random rnd = new Random();
            byte[] array = new byte[arg];
            rnd.NextBytes(array);
            return array;
        }
    }
}
