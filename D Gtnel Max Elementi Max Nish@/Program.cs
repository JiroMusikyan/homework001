﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D_Gtnel_Max_Elementi_Max_Nish_
{
    class Program
    {
        static void Main(string[] args)
        {//2. trvac e zanvac, gtnel max-i max tvanshan@;

            int[] arr = { 12, 23, 45, 67, 34 };

            #region Get Arr Max
            int arrMax = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > arrMax)
                {
                    arrMax = arr[i];
                }
            }
            #endregion

            #region Separation and Get Max
            int n = arrMax;
            int max = int.MinValue;
            while (n != 0)
            {
               int rem = n % 10;
                n /= 10;
                //Get Max
                if (rem > max)
                {
                    max = rem;
                }
            }
            #endregion

            Console.WriteLine(max);
            Console.ReadLine();

        }
    }
}
