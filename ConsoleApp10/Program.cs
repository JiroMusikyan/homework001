﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] arr = CreateRandomArray(5, 3);
            
            Console.ReadLine();
        }

        static int[,] CreateRandomArray(int n, int m)
        {
            var arr = new int[n, m];

            var rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    arr[i, j] = rnd.Next(10, 100);
                }
            }

            return arr;
        }

    }
}