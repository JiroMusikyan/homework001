﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J_Get_Sum_Jagget_Array
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] array = CreatRandomJagget(3);
            int sum = GetSum(array);
            Show(array);
            Print(sum);
            Console.ReadLine();
        }
        static int[][] CreatRandomJagget(int count)
        {
            var array = new int[count][];
            var rnd = new Random();

            for (int i = 0; i < array.Length; i++)
            {
                int n = rnd.Next(3, 8);
                array[i] = CreatRandomArray(n);
            }
            return array;
        }

        static int[] CreatRandomArray(int count)
        {
            var rnd = new Random();
            var array = new int[count];

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(10, 100);
            }
            return array;
        }

        static int GetSum(int[][] jagget)
        {
            var array = jagget;
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array[i].Length; j++)
                {
                    sum += array[i][j];
                }
            }
            return sum;
        }

        static void Show(int[][] jagger )
        {
            for (int i = 0; i < jagger.Length; i++)
            {
                for (int j = 0; j < jagger[i].Length; j++)
                {
                    Console.Write($"{jagger[i][j]} ");
                }
                Console.WriteLine("\n");
            }
        }

        static void Print(int Info)
        {
            int res = Info;
            Console.WriteLine($"Total sum in array = {res}");
        }

    }
}
