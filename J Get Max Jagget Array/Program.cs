﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J_Get_Max_Jagget_Array
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] array = CreatJaggetArray(3);
            int max = GetJaggetMax(array);
            Show(array);
            Print(max);
            

            Console.ReadLine();
        }
        
        static int[][]CreatJaggetArray(int count)
        {
            int[][] array = new int[count][];
            var rnd = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                int n = rnd.Next(5, 19);
                array[i] = CreatRadomArray(n);
            }
            return array;
        }

        static int[] CreatRadomArray (int count)
        {
            var arr = new int[count];
            var rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] =rnd.Next(5, 50);
            }
            return arr;
        }

        static int GetJaggetMax(int[][] Jag)
        {
            var array = Jag;
            int max = int.MinValue;
            for (int i = 0; i < array.Length; i++)
            {
                int check = GetMax(array[i]);
                if (check>max)
                {
                    max = check;
                }
            }
           return max;
        }
        
        static int GetMax(int[]arg)
        {
            var array = arg;
            int max = array[0];

            foreach (int item in array)
            {
                if (item>max)
                    max = item;
            }
            return max;
        }

        static void Show(int[][]jagget)
        {
            for (int i = 0; i < jagget.Length; i++)
            {
                for (int j = 0; j < jagget[i].Length; j++)
                {
                    Console.Write($"{jagget[i][j]} ");
                }
                Console.WriteLine("\n");
            }
        }
        
        static void Print(int info)
        {
            int res = info;
            Console.WriteLine($"max number is {res}");
        }
    }
}
