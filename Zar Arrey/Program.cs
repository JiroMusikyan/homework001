﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zar_Arrey
{
    class Program
    {
        static void Main(string[] args)
        {
            int counter = 60;
            Random ran = new Random();
            int[] arr = new int[6];

            #region Play 
            for (int i = 0; i < counter; i++)
            {
                int zar = ran.Next(1, 7);

                switch (zar)
                {
                    case 1:
                        {
                            arr[0] += 1;
                            break;
                        }
                    case 2:
                        {
                            arr[1] += 1;
                            break;
                        }
                    case 3:
                        {
                            arr[2] += 1;
                            break;
                        }
                    case 4:
                        {
                            arr[3] += 1;
                            break;
                        }
                    case 5:
                        {
                            arr[4] += 1;
                            break;
                        }
                    case 6:
                        {
                            arr[5] += 1;
                            break;
                        }
                }
            }
            #endregion

            #region Play Result
            int won = arr[0];
            int lost = int.MaxValue;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i]>won)
                {
                    won = arr[i];
                }
                else if (arr[i]<lost)
                {
                    lost = arr[i];
                }
               
            }
            #endregion
            //print
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i]+",");
            }
            Console.WriteLine($"\n The number that most often came out is{won}\nThe number that most often came out is{lost}");
            Console.ReadLine();
        }
    }
}
