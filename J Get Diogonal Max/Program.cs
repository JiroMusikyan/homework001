﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J_Get_Diogonal_Max
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] array = CreatRandomArray(4, 4);
            int max = GetMax(array);
            Show(array);
            Print(max);
            Console.ReadLine();
        }

        static int[,] CreatRandomArray(int x, int y)
        {
            var array = new int[x, y];

            var rnd = new Random();
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    array[i, j] = rnd.Next(9, 99);
                }
            }
            return array;
        }

        static int GetMax(int[,]arg)
        {
            int[,] array = arg;
            int x = array.GetLength(0);
            int y = array.GetLength(1);
            int max = array[0, 0];

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (i==j)
                    {
                        if (max < array[i, j])
                        {
                            max = array[i, j];
                        }
                    }
                }
            }
            return max;
        }

        static void Show(int[,]arr)
        {
            int x = arr.GetLength(0);
            int y = arr.GetLength(1);

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    Console.Write($"{arr[i,j]}  ");
                }
                Console.WriteLine("\n");
            }
        }

        static void Print(int arg)
        {
            Console.WriteLine($"max of dioganal is {arg} ");
        }
    }
}
