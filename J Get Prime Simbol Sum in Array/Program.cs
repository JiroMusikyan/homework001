﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J_Get_Prime_Simbol_Sum_in_Array
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = CreatRandomArray(11);
            int[] primeArray = GetPrimeElement(array);
            int sum = GetSum(primeArray);
            Print(sum,array);
            Console.ReadLine();

        }

        static int[]CreatRandomArray(int length)
        {
            var array = new int[length];
            var rnd = new Random();

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(100, 998);
            }
            return array;
        }

        static int[] GetPrimeElement(int[]arg)
        {
            int l = arg.Length;
            var array = arg;
            var arrayNew = new int[l];
            for (int i = 0; i < array.Length; i++)
            {
                arrayNew[i] = 0;
                    int x = 0;
                int n = array[i];
                while (n != 0)
                {
                    int rem = n % 10;
                    n /= 10;
                    bool isPrime = rem == 2 || rem == 3 || rem == 7||rem==5;
                    if (isPrime)
                    {
                        x += rem;
                    }
                }
                arrayNew[i] = x;
            }
            return arrayNew;
        }

        static int GetSum(int[]primeArray)
        {
            int sum = 0;
            var array = primeArray;
            foreach (int item in array)
            {
                sum += item;
            }
            return sum;
        }

        static void Print(int info,int []arr)
        {
            foreach (int item in arr)
            {
                Console.WriteLine($"{item} ");
            }
            int sum = info;
            Console.WriteLine($"Prim Elemenf sum of Numbers = {sum}");

        }
    }
}
