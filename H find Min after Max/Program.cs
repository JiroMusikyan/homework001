﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H_find_Min_after_Max
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input array count: ");
            int n = int.Parse(Console.ReadLine());

            int[] arr = CreateRandomArray(n);// { 1,12,45,8,9,87,2,6,5,3};

            Print(arr);

            int maxIndex = GetMaxIndex(arr);
            //
            if (maxIndex == n - 1)
            {
                Console.WriteLine("Max is a the last element");
            }
            else
            {
                int min = GetMin(arr, maxIndex + 1);
                Console.WriteLine("Index of max number in array is: {0}", maxIndex);
                Console.WriteLine("Min number after max in array is: {0}", min);
            }

            Console.ReadLine();
        }
        static public int[] CreateRandomArray(int a) //ARRAY
        {
            int[] arr = new int[a];

            Random rand = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next(0, 50);
            }
            return arr;
        }

        static void Print(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("arr{0} = {1}", i, arr[i]);
            }
        }

        static public int GetMaxIndex(int[] a)
        {
            int max = a[0];
            int index = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] > max)
                {
                    max = a[i];
                    index = i;
                }
            }
            return index;
        }

        static public int GetMin(int[] arr, int index = 0) // MIN overloading
        {
            int min = arr[index];
            for (int i = index; i < arr.Length; i++)
            {
                if (min > arr[i])
                {
                    min = arr[i];
                }
            }
            return min;
        }


    }
}
