﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G_getArayAverageMethod
{
    class Program
    {
        static int getAraEleAver (int[]arg)
        {
            int avg = 0;
            for (int i = 0; i < arg.Length; i++)
            {
                avg += arg[i];
            }
            return avg / arg.Length;
        }
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int[] arr = new int[11] ;

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(1, 56);
            }
            int average = getAraEleAver(arr);
            Console.WriteLine($"average value of array elements {average}");
            Console.ReadLine();
        }
    }
}
